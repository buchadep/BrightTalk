$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("CreateRealmTests.feature");
formatter.feature({
  "line": 1,
  "name": "Test Create Realm API",
  "description": "",
  "id": "test-create-realm-api",
  "keyword": "Feature"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 1049657342,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Create Realm API - valid name, description - HTTP201",
  "description": "",
  "id": "test-create-realm-api;create-realm-api---valid-name,-description---http201",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "User has below details:",
  "rows": [
    {
      "cells": [
        "name",
        "zzzz"
      ],
      "line": 7
    },
    {
      "cells": [
        "description",
        "description"
      ],
      "line": 8
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "Send request to create realm",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Http status code 201 received",
  "keyword": "Then "
});
formatter.match({
  "location": "Dictionary.user_has_below_details(DataTable)"
});
formatter.result({
  "duration": 169879298,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_create_realm()"
});
formatter.result({
  "duration": 891796449,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "201",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 6447370,
  "status": "passed"
});
formatter.after({
  "duration": 72148332,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 3856264,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Create Realm API - description only - missing mandatory name - HTTP400",
  "description": "",
  "id": "test-create-realm-api;create-realm-api---description-only---missing-mandatory-name---http400",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "User has below details:",
  "rows": [
    {
      "cells": [
        "description",
        "description"
      ],
      "line": 14
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "Send request to create realm",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "Http status code 400 received",
  "keyword": "Then "
});
formatter.step({
  "line": 17,
  "name": "Error conforms to xmlResource \"MissingRealmName\"",
  "keyword": "And "
});
formatter.match({
  "location": "Dictionary.user_has_below_details(DataTable)"
});
formatter.result({
  "duration": 6532240,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_create_realm()"
});
formatter.result({
  "duration": 132578265,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 129871,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "MissingRealmName",
      "offset": 31
    }
  ],
  "location": "Dictionary.error_conforms_to_xmlResource(String)"
});
formatter.result({
  "duration": 1353186,
  "status": "passed"
});
formatter.after({
  "duration": 20921,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 6388948,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Create Realm API - mandatory name already exists - duplicate name - HTTP400",
  "description": "",
  "id": "test-create-realm-api;create-realm-api---mandatory-name-already-exists---duplicate-name---http400",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 20,
  "name": "User has below details:",
  "rows": [
    {
      "cells": [
        "name",
        "name"
      ],
      "line": 21
    },
    {
      "cells": [
        "description",
        "description"
      ],
      "line": 22
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "Send request to create realm",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "User has below details:",
  "rows": [
    {
      "cells": [
        "name",
        "name"
      ],
      "line": 25
    },
    {
      "cells": [
        "description",
        "description"
      ],
      "line": 26
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "Send request to create realm",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "Http status code 400 received",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "Error conforms to xmlResource \"DuplicateRealmName\"",
  "keyword": "And "
});
formatter.match({
  "location": "Dictionary.user_has_below_details(DataTable)"
});
formatter.result({
  "duration": 12654735,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_create_realm()"
});
formatter.result({
  "duration": 103767012,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.user_has_below_details(DataTable)"
});
formatter.result({
  "duration": 8748339,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_create_realm()"
});
formatter.result({
  "duration": 104002280,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 138950,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "DuplicateRealmName",
      "offset": 31
    }
  ],
  "location": "Dictionary.error_conforms_to_xmlResource(String)"
});
formatter.result({
  "duration": 2089386,
  "status": "passed"
});
formatter.after({
  "duration": 20527,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 10014285,
  "status": "passed"
});
formatter.scenario({
  "line": 31,
  "name": "Create Realm API - name exact 100 Char - HTTP201",
  "description": "",
  "id": "test-create-realm-api;create-realm-api---name-exact-100-char---http201",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 32,
  "name": "User has below details:",
  "rows": [
    {
      "cells": [
        "name",
        "nameishavingValuenameishavingValuenameishavingValuenameishavingValuenameishavingValuenameishValue100"
      ],
      "line": 33
    },
    {
      "cells": [
        "description",
        "description"
      ],
      "line": 34
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "Send request to create realm",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "Http status code 201 received",
  "keyword": "Then "
});
formatter.match({
  "location": "Dictionary.user_has_below_details(DataTable)"
});
formatter.result({
  "duration": 12464073,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_create_realm()"
});
formatter.result({
  "duration": 96328832,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "201",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 86055,
  "status": "passed"
});
formatter.after({
  "duration": 66178209,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 2873743,
  "status": "passed"
});
formatter.scenario({
  "line": 38,
  "name": "Create Realm API - name greater than 100 Char - HTTP400",
  "description": "",
  "id": "test-create-realm-api;create-realm-api---name-greater-than-100-char---http400",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 39,
  "name": "User has below details:",
  "rows": [
    {
      "cells": [
        "name",
        "nameishavingValuenameishavingValuenameishavingValuenameishavingValuenameishavingValuenameishaValue101"
      ],
      "line": 40
    },
    {
      "cells": [
        "description",
        "description"
      ],
      "line": 41
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "Send request to create realm",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "Http status code 400 received",
  "keyword": "Then "
});
formatter.step({
  "line": 44,
  "name": "Error conforms to xmlResource \"InvalidRealmName\"",
  "keyword": "And "
});
formatter.match({
  "location": "Dictionary.user_has_below_details(DataTable)"
});
formatter.result({
  "duration": 8889658,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_create_realm()"
});
formatter.result({
  "duration": 117950200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 85265,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "InvalidRealmName",
      "offset": 31
    }
  ],
  "location": "Dictionary.error_conforms_to_xmlResource(String)"
});
formatter.result({
  "duration": 1115155,
  "status": "passed"
});
formatter.after({
  "duration": 15001,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 2814532,
  "status": "passed"
});
formatter.scenario({
  "line": 46,
  "name": "Create Realm API - description exact 255 Char - HTTP201",
  "description": "",
  "id": "test-create-realm-api;create-realm-api---description-exact-255-char---http201",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 47,
  "name": "User has below details:",
  "rows": [
    {
      "cells": [
        "name",
        "zzzz"
      ],
      "line": 48
    },
    {
      "cells": [
        "description",
        "descriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescrValue255"
      ],
      "line": 49
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 50,
  "name": "Send request to create realm",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "Http status code 201 received",
  "keyword": "Then "
});
formatter.match({
  "location": "Dictionary.user_has_below_details(DataTable)"
});
formatter.result({
  "duration": 8984791,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_create_realm()"
});
formatter.result({
  "duration": 117334398,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "201",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 111713,
  "status": "passed"
});
formatter.after({
  "duration": 58340942,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 2688213,
  "status": "passed"
});
formatter.scenario({
  "line": 53,
  "name": "Create Realm API - description greater than 255 Char - HTTP400",
  "description": "",
  "id": "test-create-realm-api;create-realm-api---description-greater-than-255-char---http400",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 54,
  "name": "User has below details:",
  "rows": [
    {
      "cells": [
        "name",
        "PAN"
      ],
      "line": 55
    },
    {
      "cells": [
        "description",
        "descriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriValue256"
      ],
      "line": 56
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 57,
  "name": "Send request to create realm",
  "keyword": "And "
});
formatter.step({
  "line": 58,
  "name": "Http status code 400 received",
  "keyword": "Then "
});
formatter.step({
  "line": 59,
  "name": "Error conforms to xmlResource \"InvalidRealmDescription\"",
  "keyword": "And "
});
formatter.match({
  "location": "Dictionary.user_has_below_details(DataTable)"
});
formatter.result({
  "duration": 7116068,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_create_realm()"
});
formatter.result({
  "duration": 77581998,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 1663455,
  "error_message": "java.lang.AssertionError: \nExpected: a string containing \"400\"\n     but: was \"HTTP/1.1 500 Internal Server Error\"\r\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\r\n\tat org.junit.Assert.assertThat(Assert.java:865)\r\n\tat org.junit.Assert.assertThat(Assert.java:832)\r\n\tat com.brighttalkdemo.cucumber.glue.stepdefinitions.Dictionary.http_status_code_received(Dictionary.java:92)\r\n\tat ✽.Then Http status code 400 received(CreateRealmTests.feature:58)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "InvalidRealmDescription",
      "offset": 31
    }
  ],
  "location": "Dictionary.error_conforms_to_xmlResource(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 13421,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 2944403,
  "status": "passed"
});
formatter.scenario({
  "line": 61,
  "name": "Create Realm API - Content-Type header other than application-xml - HTTP415",
  "description": "",
  "id": "test-create-realm-api;create-realm-api---content-type-header-other-than-application-xml---http415",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 62,
  "name": "User has below details:",
  "rows": [
    {
      "cells": [
        "name",
        "PAN"
      ],
      "line": 63
    },
    {
      "cells": [
        "description",
        "description"
      ],
      "line": 64
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 65,
  "name": "set header as below:",
  "rows": [
    {
      "cells": [
        "Content-Type",
        "blabla"
      ],
      "line": 66
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 67,
  "name": "Send request to create realm",
  "keyword": "And "
});
formatter.step({
  "line": 68,
  "name": "Http status code 415 received",
  "keyword": "Then "
});
formatter.match({
  "location": "Dictionary.user_has_below_details(DataTable)"
});
formatter.result({
  "duration": 12449073,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.set_header_as_below(DataTable)"
});
formatter.result({
  "duration": 37106,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_create_realm()"
});
formatter.result({
  "duration": 98004129,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "415",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 112897,
  "status": "passed"
});
formatter.after({
  "duration": 13816,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 3493099,
  "status": "passed"
});
formatter.scenario({
  "line": 70,
  "name": "Create Realm API - Invalid endpoint - Method not allowed - HTTP405",
  "description": "",
  "id": "test-create-realm-api;create-realm-api---invalid-endpoint---method-not-allowed---http405",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 71,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realmn\"",
  "keyword": "Given "
});
formatter.step({
  "line": 72,
  "name": "User has below details:",
  "rows": [
    {
      "cells": [
        "name",
        "PAN"
      ],
      "line": 73
    },
    {
      "cells": [
        "description",
        "description"
      ],
      "line": 74
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 75,
  "name": "Send request to create realm",
  "keyword": "And "
});
formatter.step({
  "line": 76,
  "name": "Http status code 405 received",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realmn",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 618171,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.user_has_below_details(DataTable)"
});
formatter.result({
  "duration": 10627325,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_create_realm()"
});
formatter.result({
  "duration": 77090934,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "405",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 311849,
  "error_message": "java.lang.AssertionError: \nExpected: a string containing \"405\"\n     but: was \"HTTP/1.1 404 Not Found\"\r\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\r\n\tat org.junit.Assert.assertThat(Assert.java:865)\r\n\tat org.junit.Assert.assertThat(Assert.java:832)\r\n\tat com.brighttalkdemo.cucumber.glue.stepdefinitions.Dictionary.http_status_code_received(Dictionary.java:92)\r\n\tat ✽.Then Http status code 405 received(CreateRealmTests.feature:76)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 15000,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 2639660,
  "status": "passed"
});
formatter.scenario({
  "line": 78,
  "name": "Create Realm API - Accept headed other than application-xml - Not Acceptable - HTTP406",
  "description": "",
  "id": "test-create-realm-api;create-realm-api---accept-headed-other-than-application-xml---not-acceptable---http406",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 79,
  "name": "User has below details:",
  "rows": [
    {
      "cells": [
        "name",
        "PAN"
      ],
      "line": 80
    },
    {
      "cells": [
        "description",
        "description"
      ],
      "line": 81
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 82,
  "name": "set header as below:",
  "rows": [
    {
      "cells": [
        "Content-Type",
        "application/xml;charset\u003d\"utf-8\""
      ],
      "line": 83
    },
    {
      "cells": [
        "Accept",
        "application/txt"
      ],
      "line": 84
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "Send request to create realm",
  "keyword": "And "
});
formatter.step({
  "line": 86,
  "name": "Http status code 406 received",
  "keyword": "Then "
});
formatter.match({
  "location": "Dictionary.user_has_below_details(DataTable)"
});
formatter.result({
  "duration": 17047457,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.set_header_as_below(DataTable)"
});
formatter.result({
  "duration": 39080,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_create_realm()"
});
formatter.result({
  "duration": 79304664,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "406",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 150003,
  "status": "passed"
});
formatter.after({
  "duration": 15396,
  "status": "passed"
});
formatter.uri("GetRealmTests.feature");
formatter.feature({
  "line": 1,
  "name": "Test Get Realm API",
  "description": "",
  "id": "test-get-realm-api",
  "keyword": "Feature"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 4357195,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Get Realm by id - valid response - HTTP200",
  "description": "",
  "id": "test-get-realm-api;get-realm-by-id---valid-response---http200",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "User has below details:",
  "rows": [
    {
      "cells": [
        "name",
        "zzzz200"
      ],
      "line": 7
    },
    {
      "cells": [
        "description",
        "description"
      ],
      "line": 8
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "Send request to create realm",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Used id from earlier create realm request",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "Send request to get realm",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "Http status code 200 received",
  "keyword": "Then "
});
formatter.match({
  "location": "Dictionary.user_has_below_details(DataTable)"
});
formatter.result({
  "duration": 11762611,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_create_realm()"
});
formatter.result({
  "duration": 100411680,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.use_id_from_earlier_create_realm_request()"
});
formatter.result({
  "duration": 33948,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_get_realm()"
});
formatter.result({
  "duration": 755147,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat com.brighttalkdemo.cucumber.glue.stepdefinitions.Dictionary.send_request_to_get_realm(Dictionary.java:110)\r\n\tat ✽.And Send request to get realm(GetRealmTests.feature:11)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 14211,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 3844027,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Get Realm by id - id does not exists - RealmNotFound - HTTP404",
  "description": "",
  "id": "test-get-realm-api;get-realm-by-id---id-does-not-exists---realmnotfound---http404",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 15,
  "name": "User has realm id as \"1\"",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "Send request to get realm",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "Http status code 404 received",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "Error conforms to xmlResource \"RealmNotFound\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 22
    }
  ],
  "location": "Dictionary.user_has_realm_id_as(String)"
});
formatter.result({
  "duration": 70659,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_get_realm()"
});
formatter.result({
  "duration": 82363543,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "404",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 339086,
  "error_message": "java.lang.AssertionError: \nExpected: a string containing \"404\"\n     but: was \"HTTP/1.1 400 Bad Request\"\r\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\r\n\tat org.junit.Assert.assertThat(Assert.java:865)\r\n\tat org.junit.Assert.assertThat(Assert.java:832)\r\n\tat com.brighttalkdemo.cucumber.glue.stepdefinitions.Dictionary.http_status_code_received(Dictionary.java:92)\r\n\tat ✽.Then Http status code 404 received(GetRealmTests.feature:17)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "RealmNotFound",
      "offset": 31
    }
  ],
  "location": "Dictionary.error_conforms_to_xmlResource(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 18553,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 3264936,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Get Realm by id -  - Invalid endpoint - Method not allowed - HTTP405",
  "description": "",
  "id": "test-get-realm-api;get-realm-by-id------invalid-endpoint---method-not-allowed---http405",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 21,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.step({
  "line": 22,
  "name": "User has realm id as \"\"",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "Send request to get realm",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "Http status code 405 received",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 479616,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 22
    }
  ],
  "location": "Dictionary.user_has_realm_id_as(String)"
});
formatter.result({
  "duration": 61580,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_get_realm()"
});
formatter.result({
  "duration": 72137674,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "405",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 143292,
  "status": "passed"
});
formatter.after({
  "duration": 14605,
  "status": "passed"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 3202171,
  "status": "passed"
});
formatter.scenario({
  "line": 26,
  "name": "Get Realm by id - Accept header other than xml - Not Acceptable - HTTP406",
  "description": "",
  "id": "test-get-realm-api;get-realm-by-id---accept-header-other-than-xml---not-acceptable---http406",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 27,
  "name": "set header as below:",
  "rows": [
    {
      "cells": [
        "Content-Type",
        "application/xml;charset\u003d\"utf-8\""
      ],
      "line": 28
    },
    {
      "cells": [
        "Accept",
        "application/txt"
      ],
      "line": 29
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "User has realm id as \"1\"",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "Send request to get realm",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "Http status code 406 received",
  "keyword": "Then "
});
formatter.match({
  "location": "Dictionary.set_header_as_below(DataTable)"
});
formatter.result({
  "duration": 42238,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 22
    }
  ],
  "location": "Dictionary.user_has_realm_id_as(String)"
});
formatter.result({
  "duration": 92765,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_get_realm()"
});
formatter.result({
  "duration": 86743634,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "406",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 125924,
  "status": "passed"
});
formatter.after({
  "duration": 15790,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 34,
  "name": "Get Realm by id - id not in (1-9999) - InvalidRealmId - HTTP400",
  "description": "",
  "id": "test-get-realm-api;get-realm-by-id---id-not-in-(1-9999)---invalidrealmid---http400",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 35,
  "name": "User has realm id as \"\u003cid\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "Send request to get realm",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "Http status code 400 received",
  "keyword": "Then "
});
formatter.step({
  "line": 38,
  "name": "Error conforms to xmlResource \"InvalidRealmId\"",
  "keyword": "And "
});
formatter.examples({
  "line": 40,
  "name": "",
  "description": "",
  "id": "test-get-realm-api;get-realm-by-id---id-not-in-(1-9999)---invalidrealmid---http400;",
  "rows": [
    {
      "cells": [
        "id"
      ],
      "line": 41,
      "id": "test-get-realm-api;get-realm-by-id---id-not-in-(1-9999)---invalidrealmid---http400;;1"
    },
    {
      "cells": [
        "10000"
      ],
      "line": 42,
      "id": "test-get-realm-api;get-realm-by-id---id-not-in-(1-9999)---invalidrealmid---http400;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "System endpoint as \"http://recruit01.test01.brighttalk.net:8080/user/realm\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "http://recruit01.test01.brighttalk.net:8080/user/realm",
      "offset": 20
    }
  ],
  "location": "Dictionary.system_endpoint(String)"
});
formatter.result({
  "duration": 2837822,
  "status": "passed"
});
formatter.scenario({
  "line": 42,
  "name": "Get Realm by id - id not in (1-9999) - InvalidRealmId - HTTP400",
  "description": "",
  "id": "test-get-realm-api;get-realm-by-id---id-not-in-(1-9999)---invalidrealmid---http400;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 35,
  "name": "User has realm id as \"10000\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "Send request to get realm",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "Http status code 400 received",
  "keyword": "Then "
});
formatter.step({
  "line": 38,
  "name": "Error conforms to xmlResource \"InvalidRealmId\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "10000",
      "offset": 22
    }
  ],
  "location": "Dictionary.user_has_realm_id_as(String)"
});
formatter.result({
  "duration": 63159,
  "status": "passed"
});
formatter.match({
  "location": "Dictionary.send_request_to_get_realm()"
});
formatter.result({
  "duration": 118357972,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 17
    }
  ],
  "location": "Dictionary.http_status_code_received(int)"
});
formatter.result({
  "duration": 347771,
  "error_message": "java.lang.AssertionError: \nExpected: a string containing \"400\"\n     but: was \"HTTP/1.1 500 Internal Server Error\"\r\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\r\n\tat org.junit.Assert.assertThat(Assert.java:865)\r\n\tat org.junit.Assert.assertThat(Assert.java:832)\r\n\tat com.brighttalkdemo.cucumber.glue.stepdefinitions.Dictionary.http_status_code_received(Dictionary.java:92)\r\n\tat ✽.Then Http status code 400 received(GetRealmTests.feature:37)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "InvalidRealmId",
      "offset": 31
    }
  ],
  "location": "Dictionary.error_conforms_to_xmlResource(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 13816,
  "status": "passed"
});
});