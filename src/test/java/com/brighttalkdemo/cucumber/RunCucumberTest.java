package com.brighttalkdemo.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = { "pretty","html:cucumber-html-reports", "json:cucumber-html-reports/cucumber.json", "junit:cucumber-html-reports/cucumber-junit.xml"},
        glue = "com.brighttalkdemo.cucumber.glue.stepdefinitions",
        features = "src\\test\\resources\\com\\brighttalkdemo\\cucumber\\features\\TestSuit\\"
)
public class RunCucumberTest {
}
