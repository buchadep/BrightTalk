Feature: Test Get Realm API
  Background:
    Given System endpoint as "http://recruit01.test01.brighttalk.net:8080/user/realm"

    Scenario: Get Realm by id - valid response - HTTP200
      Given User has below details:
        |name      |zzzz200|
        |description      |description|
      And Send request to create realm
      When Used id from earlier create realm request
      And Send request to get realm
      Then Http status code 200 received

    Scenario: Get Realm by id - id does not exists - RealmNotFound - HTTP404
      When User has realm id as "1"
      And Send request to get realm
      Then Http status code 404 received
      And Error conforms to xmlResource "RealmNotFound"

    Scenario: Get Realm by id -  - Invalid endpoint - Method not allowed - HTTP405
      Given System endpoint as "http://recruit01.test01.brighttalk.net:8080/user/realm"
      When User has realm id as ""
      And Send request to get realm
      Then Http status code 405 received

    Scenario: Get Realm by id - Accept header other than xml - Not Acceptable - HTTP406
      And set header as below:
          |Content-Type|application/xml;charset="utf-8"|
          |Accept      |  application/txt    |
      When User has realm id as "1"
      And Send request to get realm
      Then Http status code 406 received

    Scenario Outline: Get Realm by id - id not in (1-9999) - InvalidRealmId - HTTP400
      When User has realm id as "<id>"
      And Send request to get realm
      Then Http status code 400 received
      And Error conforms to xmlResource "InvalidRealmId"

    Examples:
      |id|
      |10000|
