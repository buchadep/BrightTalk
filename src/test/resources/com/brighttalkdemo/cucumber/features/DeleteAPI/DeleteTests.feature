Feature: Testing Delete Realm API here
  Background:
    Given System endpoint as "http://recruit01.test01.brighttalk.net:8080/user/realm"

    Scenario: Delete realm by id - Delete all range 1-9999
      When Delete all realm range (1,9999)

