package com.brighttalkdemo.cucumber.glue.stepdefinitions;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.co.v2systems.framework.utils.Methods;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

//This Class implements all the Dictionary Statements
public class Dictionary {
    HttpRequestHandler httpRequestHandler;
    XmlResource xmlResource_send;
    XmlResource xmlResource_rec;
    List<List<String>> inputDetails;
    String id;
    List<List<String>> headerList;
    List<List<String>> analysedResponseList;
    List<List<String>> errorMessages;
    Properties errorConfig;
    String httpStatus;
    String endpointUrl;
    static Logger logger = LogManager.getLogger(Dictionary.class);

//Constructor
    public Dictionary() {
        loadConfigProperties();
        httpRequestHandler = new HttpRequestHandler();
        xmlResource_send = new XmlResource(Methods.getApplicationPath() + "\\logs\\postxml.xml");
        xmlResource_rec = new XmlResource(Methods.getApplicationPath() + "\\logs\\responsexml.xml");
        endpointUrl = "http://recruit01.test01.brighttalk.net:8080/user/realm"; //default
    }
    public void loadConfigProperties(){
        errorConfig=Utils.readConfigProperties(Dictionary.class.getResource("/errorConfig.properties").getFile());
        errorMessages = new ArrayList<List<String>>();
        Set<Object> keys = errorConfig.keySet();
        for(Object k:keys){
            String key = (String)k;
            List<String> error = new ArrayList<String>();
            error.add(0,key);error.add(1,errorConfig.getProperty(key));
            errorMessages.add(error);
            logger.info("loading error definitions : "+ key+": "+errorConfig.getProperty(key));
        }
    }
//-------- Class Methods-------//
//Set portal URL
    @Given("^System endpoint as \"([^\"]*)\"$")
    public void system_endpoint(String url) throws Throwable {
        endpointUrl = url;
        logger.info("set endpoint url: " + endpointUrl);
    }
//Read all the Variable for create realm API
    @When("^User has below details:$")
    public void user_has_below_details(DataTable userDetailsDt) throws Throwable {
        inputDetails = userDetailsDt.raw();
        xmlResource_send = xmlResource_send.creatRealmPostXml(inputDetails, xmlResource_send);
    }
//Read all the Variable for create realm API
    @When("^User has realm id as \"([^\"]*)\"$")
    public void user_has_realm_id_as(String id) throws Throwable {
        this.id = id;
    }
//Send http request create realm
    @And("^Send request to create realm$")
    public void send_request_to_create_realm() throws Throwable {
        httpStatus = httpRequestHandler.post(endpointUrl, "", xmlResource_send.getXmlPath(), xmlResource_rec.getXmlPath());
        assertTrue(httpStatus != null);
        xmlResource_rec.printXmlResource();
        analysedResponseList=xmlResource_rec.analyseResponse(httpStatus);
    }
//
    @Then("^Used id from earlier create realm request$")
    public void use_id_from_earlier_create_realm_request() throws Throwable {
        id=Utils.getValueByKey(analysedResponseList,"id");
    }
//Validate received http status code (Used Integer to demo)
    @Then("^Http status code (\\d+) received$")
    public void http_status_code_received(int expectedStatusCode) throws Throwable {
        assertThat(httpStatus, containsString(Integer.toString(expectedStatusCode)));
    }
//Response Validation
    @Then("^Error conforms to xmlResource \"([^\"]*)\"$")
    public void error_conforms_to_xmlResource(String xmlResponse) throws Throwable {
        assertThat(Utils.getValueByKey(analysedResponseList,"code"), containsString(xmlResponse));
        assertThat(Utils.getValueByKey(analysedResponseList, "message"), containsString(Utils.getValueByKey(errorMessages, xmlResponse)));
    }
//Set Headers
    @When("^set header as below:$")
    public void set_header_as_below(DataTable headersDt) throws Throwable {
        headerList = headersDt.raw();
        httpRequestHandler.setContentType(headerList);
    }
//http get realm API request
    @When("^Send request to get realm$")
    public void send_request_to_get_realm() throws Throwable {
        httpStatus = httpRequestHandler.get(endpointUrl, id, null, xmlResource_rec.getXmlPath());
        assertTrue(httpStatus != null);
        xmlResource_rec.printXmlResource();
        analysedResponseList=xmlResource_rec.analyseResponse(httpStatus);
    }
//Delete all realm
    @When("^Delete all realm range \\((\\d+),(\\d+)\\)$")
     public void delete_all_realm_range(int startId, int endId) throws Throwable {
        for (int i=startId; i<=endId; i++){
            httpStatus = httpRequestHandler.delete(endpointUrl, Integer.toString(i) );
        }
    }
//After Every Scenario do clean up
    @After
    public void afterScenario() {
        if(httpStatus!=null) {
            if(httpStatus.contains("201")) {
                httpStatus = httpRequestHandler.delete(endpointUrl, Utils.getValueByKey(analysedResponseList, "id"));
                logger.info("cleaning after each scenario with http 201....");
            }
        }
        //Set headers to Default in case Modified
        httpRequestHandler.setContentType(null);

    }

}
