package com.brighttalkdemo.cucumber.glue.stepdefinitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.co.v2systems.framework.files.CustomXml;
import uk.co.v2systems.framework.utils.KeyValuePair;
import uk.co.v2systems.framework.utils.Methods;

import java.util.ArrayList;
import java.util.List;

public class XmlResource {
   CustomXml xml;
   private String xmlPath;
   static Logger logger = LogManager.getLogger(XmlResource.class);
//Constructor
   public XmlResource(String xmlPath){
       this.xmlPath= xmlPath;
       xml = new CustomXml();
   }
    public void setXmlPath(String xmlPath) {
        this.xmlPath = xmlPath;
        xml.openXml(xmlPath);
    }
    public String getXmlPath() {
        return xmlPath;
    }
    public void printXmlResource(){
        xml.openXml(xmlPath);
        xml.printXml();
    }
    public String getAttribute(String xpath, String attribute){
        return xml.findElementsByXpath(xpath).item(0).getAttributes().getNamedItem(attribute).getNodeValue();
    }
    public String getNodeValue(String xpath){
        if(xml.findElementsByXpath(xpath).item(0)!=null)
            return xml.findElementsByXpath(xpath).item(0).getTextContent();
        return "";
    }
    public List<List<String>> analyseResponse(String statusCode){
        List<List<String>> analysedResponseList = new ArrayList<List<String>>();
        if(statusCode.contains("201")) {
            this.printXmlResource();
            List<String> element = new ArrayList<String>();
            element.add(0,"id");
            element.add(1,this.getAttribute("//*[@id]", "id"));
            analysedResponseList.add(element);
            logger.info("rec_xml id: " + this.getAttribute("//*[@id]", "id"));
            element = new ArrayList<String>();
            element.add(0,"name");
            element.add(1,this.getAttribute("//*[@name]", "name"));
            analysedResponseList.add(element);
            logger.info("rec_xml name: " + this.getAttribute("//*[@name]", "name"));
            if(this.getNodeValue("//description")!=null) {
                element = new ArrayList<String>();
                element.add(0, "description");
                element.add(1, this.getNodeValue("//description"));
                analysedResponseList.add(element);
                logger.info("rec_xml description: " + this.getNodeValue("//description"));
            }
            element = new ArrayList<String>();
            element.add(0,"key");
            element.add(1,this.getNodeValue("//key"));
            analysedResponseList.add(element);
            logger.info("rec_xml key: " + this.getNodeValue("//key"));
            return analysedResponseList;
        }else {
            List<String> element = new ArrayList<String>();
            element.add(0,"code");
            element.add(1,this.getNodeValue("//code"));
            analysedResponseList.add(element);
            logger.info("rec_xml code: " + this.getNodeValue("//code"));
            element = new ArrayList<String>();
            element.add(0,"message");
            element.add(1,this.getNodeValue("//message"));
            analysedResponseList.add(element);
            logger.info("rec_xml message: " + this.getNodeValue("//message"));
            return analysedResponseList;
        }
    }
    public XmlResource creatRealmPostXml(List<List<String>> inputDetails, XmlResource xmlResource_send){
        List<KeyValuePair> kvpl = new ArrayList<KeyValuePair>();
        xmlResource_send.xml.openXml();
        xmlResource_send.xml.addXmlTagbyXpath("realm", kvpl, "", "/*");
        //
        for(int i=0; i < inputDetails.size(); i++) {
            KeyValuePair kvp = new KeyValuePair();
            if(inputDetails.get(i).get(0).equalsIgnoreCase("name")) {
                kvp.setKeyValue(inputDetails.get(i).get(0)+","+inputDetails.get(i).get(1),',');
                kvpl.add(kvp);
                xmlResource_send.xml.updateElementAttributeValue(xmlResource_send.xml.findElementsByXpath("/realm"), "name", inputDetails.get(i).get(1));
                logger.info(inputDetails.get(i).get(0) + " : [ " + inputDetails.get(i).get(1) + " ]");
            }
            if(inputDetails.get(i).get(0).equalsIgnoreCase("description")) {
                xmlResource_send.xml.addXmlTagbyXpath("description", null, inputDetails.get(i).get(1), "/realm");
                //xmlResource_send.xml.writeXml(xmlResource_send.getXmlPath());
                logger.info(inputDetails.get(i).get(0) + " : [ " + inputDetails.get(i).get(1) + " ]");
            }
        }
        xmlResource_send.xml.writeXml(xmlResource_send.getXmlPath());
        return xmlResource_send;
    }
}
