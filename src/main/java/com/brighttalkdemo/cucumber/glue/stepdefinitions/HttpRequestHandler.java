package com.brighttalkdemo.cucumber.glue.stepdefinitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.co.v2systems.framework.http.CustomHttpClient;
import uk.co.v2systems.framework.utils.Methods;

import java.util.List;

public class HttpRequestHandler {
    static Logger logger = LogManager.getLogger(HttpRequestHandler.class);
    private String lastHttpResponse="";
    private List<List<String>> headerList;
    private String contentType="application/xml;charset=\"utf-8\"";
    CustomHttpClient httpClient;
//------------Method Implementation-----------//
    public String post(String endpointUrl,String apiPrefix, String sendFilePath, String recFilePath){
        try{
            httpClient = new CustomHttpClient(endpointUrl);
            if(!apiPrefix.equalsIgnoreCase(""))
                endpointUrl = endpointUrl +"/" +apiPrefix;
            httpClient.setUrl(endpointUrl);
            httpClient.setSendFile(sendFilePath);
            httpClient.setResponseFile(recFilePath); //logging
            if(headerList!=null){
                for(int i=0; i< headerList.size();i++){
                    httpClient.setHeader(headerList.get(i).get(0), headerList.get(i).get(1));
                }
            }else {//Default Headers
                httpClient.setHeader("Content-Type", contentType);
            }
            httpClient.post(false);
            lastHttpResponse = httpClient.getLastHttpRequestStatus();
            logger.info("#################POST#####################################");
            logger.info("post uri: "+ endpointUrl);
            logger.info("post file: "+ sendFilePath);
            logger.info("resp file: "+ recFilePath);
            logger.info("http post response: " + lastHttpResponse);
            return lastHttpResponse;
        }catch(Exception e){
            logger.error(e.toString());
            return null;
        }

    }
    public String get(String endpointUrl,String apiPrefix, String sendFilePath, String recFilePath){
        try{
            httpClient = new CustomHttpClient(endpointUrl);
            if(!apiPrefix.equalsIgnoreCase(""))
                endpointUrl = endpointUrl +"/" +apiPrefix;
            httpClient.setUrl(endpointUrl);
            if(sendFilePath!=null)
                httpClient.setSendFile(sendFilePath);
            httpClient.setResponseFile(recFilePath); //logging
            //Default Headers
            if(headerList!=null){
                for(int i=0; i< headerList.size();i++){
                    httpClient.setHeader(headerList.get(i).get(0), headerList.get(i).get(1));
                }
            }else {//Default Headers
                httpClient.setHeader("Content-Type", contentType);
            }
            httpClient.get(false);
            lastHttpResponse = httpClient.getLastHttpRequestStatus();
            logger.info("#################POST#####################################");
            logger.info("get uri: "+ endpointUrl);
            logger.info("get file: "+ sendFilePath);
            logger.info("get file: "+ recFilePath);
            logger.info("http get response: " + lastHttpResponse);
            return lastHttpResponse;
        }catch(Exception e){
            logger.error(e.toString());
            return null;
        }

    }

    public String delete(String endpointUrl,String apiPrefix){
        try{
            httpClient = new CustomHttpClient(endpointUrl);
            if(!apiPrefix.equalsIgnoreCase(""))
                endpointUrl = endpointUrl + "/" + apiPrefix;
            httpClient.setUrl(endpointUrl);
            //httpClient.setSendFile(sendFilePath);
            //httpClient.setResponseFile(recFilePath); //logging
            //Default Headers
            httpClient.setHeader("Content-Type", contentType);
            httpClient.delete(false);
            lastHttpResponse = httpClient.getLastHttpRequestStatus();
            logger.info("###################DELETE################################");
            logger.info("delete req uri: "+ endpointUrl);
            //logger.info("delete req send file: "+ sendFilePath);
            //logger.info("resp req rec file: "+ recFilePath);
            logger.info("http delete response: " + lastHttpResponse);
            return lastHttpResponse;
        }catch(Exception e){
            logger.error(e.toString());
            return null;
        }

    }
    public String getLastHttpResponse() {
        return lastHttpResponse;
    }
    public List<List<String>> getContentType() {
        return headerList;
    }
    public void setContentType(List<List<String>> headerList) {
        this.headerList = headerList;
    }
}
