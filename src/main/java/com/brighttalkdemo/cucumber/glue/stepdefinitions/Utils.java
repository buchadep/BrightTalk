package com.brighttalkdemo.cucumber.glue.stepdefinitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class Utils {
    static Logger logger = LogManager.getLogger(Dictionary.class);
    public Utils(){

    }
    public static String getValueByKey(List<List<String>> userDetails, String key){
       for(int i=0; i< userDetails.size(); i++){
           if(userDetails.get(i).get(0).equals(key)){
               logger.info("[ "+ key + "] returned value: " +userDetails.get(i).get(1));
               return userDetails.get(i).get(1); //Return Value for the Key
           }
        }
        return null;
    }
    public static Properties readConfigProperties(String configPath){
        Properties properitis = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(configPath);
            // load a properties file
            properitis.load(input);
            logger.info("Successfully loaded config " + configPath);
            return properitis;
        }catch (Exception e){
            logger.error("Unable to read config.properties "+ e.toString());
            return null;
        }

    }
}
