Feature: Test Create Realm API
  Background:
    Given System endpoint as "http://recruit01.test01.brighttalk.net:8080/user/realm"

    Scenario: Create Realm API - valid name, description - HTTP201
      When User has below details:
        |name      |zzzz|
        |description      |description|
      And Send request to create realm
      Then Http status code 201 received

    Scenario: Create Realm API - description only - missing mandatory name - HTTP400
      When User has below details:
        |description      |description|
      And Send request to create realm
      Then Http status code 400 received
      And Error conforms to xmlResource "MissingRealmName"

    Scenario: Create Realm API - mandatory name already exists - duplicate name - HTTP400
      When User has below details:
        |name      |name|
        |description      |description|
      And Send request to create realm
      When User has below details:
        |name      |name|
        |description      |description|
      And Send request to create realm
      Then Http status code 400 received
      And Error conforms to xmlResource "DuplicateRealmName"

    Scenario: Create Realm API - name exact 100 Char - HTTP201
      When User has below details:
        |name      |nameishavingValuenameishavingValuenameishavingValuenameishavingValuenameishavingValuenameishValue100|
        |description      |description|
      And Send request to create realm
      Then Http status code 201 received

    Scenario: Create Realm API - name greater than 100 Char - HTTP400
      When User has below details:
        |name      |nameishavingValuenameishavingValuenameishavingValuenameishavingValuenameishavingValuenameishaValue101|
        |description      |description|
      And Send request to create realm
      Then Http status code 400 received
      And Error conforms to xmlResource "InvalidRealmName"

    Scenario: Create Realm API - description exact 255 Char - HTTP201
      When User has below details:
        |name      |zzzz|
        |description      |descriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescrValue255|
      And Send request to create realm
      Then Http status code 201 received

    Scenario: Create Realm API - description greater than 255 Char - HTTP400
    When User has below details:
        |name      |PAN|
        |description      |descriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriptionhavingValuedescriValue256|
      And Send request to create realm
      Then Http status code 400 received
      And Error conforms to xmlResource "InvalidRealmDescription"

  Scenario: Create Realm API - Content-Type header other than application-xml - HTTP415
    When User has below details:
      |name      |PAN|
      |description      |description|
    And set header as below:
      |Content-Type|blabla|
    And Send request to create realm
    Then Http status code 415 received

    Scenario: Create Realm API - Invalid endpoint - Method not allowed - HTTP405
      Given System endpoint as "http://recruit01.test01.brighttalk.net:8080/user/realmn"
      When User has below details:
        |name      |PAN|
        |description      |description|
      And Send request to create realm
      Then Http status code 405 received

  Scenario: Create Realm API - Accept headed other than application-xml - Not Acceptable - HTTP406
    When User has below details:
      |name      |PAN|
      |description      |description|
    And set header as below:
      |Content-Type|application/xml;charset="utf-8"|
      |Accept      |  application/txt    |
    And Send request to create realm
    Then Http status code 406 received